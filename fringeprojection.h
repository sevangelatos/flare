#ifndef FRINGEPROJECTION_H
#define FRINGEPROJECTION_H

#include "plain_image.h"
#include <cstdio>

namespace FringeProjection
{
    struct PhasePixel
    {
        float     phase;
        uint8_t   rgb[3];
        uint8_t   confidence;  ///< Pixel confidence level
    };

    void FloydSteinberg(PlainImage destImg, const PlainImage sourceImg);
    bool GenerateCosinePattern(const PlainImage img, float period, float phaseShift);
    void SynthesizeWrappedPhaseImage(PlainImage wrapped, const PlainImage img[3]);
    bool PhaseImageRange(const PlainImage img, float *minVal, float *maxVal);
    void PhaseImageToGrayAlpha(PlainImage dst, const PlainImage img, float minVal, float maxVal, int transparencyThreshold);
    void UnwrapPhaseImage(PlainImage img, int minConfidence);

    /// a.phase -= b.phase; a.confidence = min(a.confidence, b.confidence)
    void PhaseImageSubtractInPlace(PlainImage a, const PlainImage b);

    void PhaseImageToAscii(FILE *out, const PlainImage img, int minConfidence, bool mixdown, float zScale);
}

#endif // FRINGEPROJECTION_H
