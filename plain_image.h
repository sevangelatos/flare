#ifndef PLAIN_IMAGE_H
#define PLAIN_IMAGE_H

#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>


#ifdef USE_OPENCV_CPP
#include <opencv/cxcore.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif

    typedef struct PlainImage_s *PlainImage;

    struct PlainImage_s
    {
        int width;                // Image width
        int height;               // Image height
        int bytes_per_sample;     // How many bytes do we accupy for each pixel? (=3 if RGB_bit8)
        int channels;             // Number of image channels
        uint8_t **pixels;         // The actual image data (actual sample data type might not be uchars)
        void *buffer;             // The original pointer that we should free.
        bool continuous;          // Are the image data allocated in a continuous buffer?
        bool is_420v;             // If true then pixels[] has an additional (height/2) rows, each with (width/2) pairs of Cb,Cr (1 byte per component)
        int  _refcount;           // Reference counter for this image
        void *_align[];           // Just to ease alignment of image data after the main struct
    } __attribute__ ((aligned (16)));

    bool PlainImage_RunTests();

    PlainImage PlainImage_Create(int width, int height, int bytes_per_sample, int channels);
    PlainImage PlainImage_Create420v(int width, int height);
    PlainImage PlainImage_CreateFrom420v( int width, int height, const uint8_t *luma, int luma_stride, const uint8_t *chroma, int chroma_stride );

    PlainImage PlainImage_Resize(PlainImage img, int width, int height);

    void PlainImage_CopyRect( PlainImage src, int src_x, int src_y, PlainImage dest, int dest_x, int dest_y, int width, int height );

    PlainImage PlainImage_RGBFromBGRX8_C(int width, int height, const uint8_t *src);
    PlainImage PlainImage_RGBFrom420v_C(int width, int height, const uint8_t *luma, int luma_stride, const uint8_t *chroma, int chroma_stride );

    PlainImage PlainImage_420vToRGB_C( PlainImage yuv );
    PlainImage PlainImage_RGBTo420v( PlainImage rgb );

    //Specifically for ingesting Apple's preferred video frame format
    void PlainImage_RGBToBGR(PlainImage img);

    void PlainImage_RGBMergeA(PlainImage dest, PlainImage rgb, PlainImage a);

#ifdef USE_OPENCV_CPP

    bool PlainImage_ShallowCopyIplImage(PlainImage *pdst, IplImage *src);

    bool PlainImage_DeepCopyIplImage(PlainImage *pdst, IplImage *src);

    IplImage *PlainImage_ToIplImage(const PlainImage img);

#endif

    bool PlainImage_SavePng(const PlainImage img, const char *filename);

    PlainImage PlainImage_LoadPng(const char *filename);

    void PlainImage_Integral_C(PlainImage integral, PlainImage input);

    void PlainImage_Min_C(PlainImage dest, PlainImage src1, PlainImage src2);
    void PlainImage_Max_C(PlainImage dest, PlainImage src1, PlainImage src2);

    void PlainImage_AbsGrayDiff420v_C(PlainImage dst, PlainImage a, PlainImage b);
    void PlainImage_AbsGrayDiff_C(PlainImage dst, PlainImage a, PlainImage b);

    void PlainImage_BitwiseAnd(PlainImage img, uint8_t bitmask);

    bool PlainImage_Rgb2Grayscale( PlainImage im );

#ifdef __ARM_NEON__
    PlainImage PlainImage_RGBFromBGRX8_NEON(int width, int height, const uint8_t *src);
    PlainImage PlainImage_420vToRGB_NEON( PlainImage yuv );
    void PlainImage_Integral_NEON(PlainImage integral, PlainImage input);
    void PlainImage_Min_NEON(PlainImage dest, PlainImage src1, PlainImage src2);
    void PlainImage_Max_NEON(PlainImage dest, PlainImage src1, PlainImage src2);
    void PlainImage_AbsGrayDiff420v_NEON(PlainImage dest, PlainImage src1, PlainImage src2);
    void PlainImage_AbsGrayDiff_NEON(PlainImage dest, PlainImage src1, PlainImage src2);
    #define PlainImage_RGBFromBGRX8 PlainImage_RGBFromBGRX8_C
    #define PlainImage_420vToRGB PlainImage_420vToRGB_NEON
    #define PlainImage_Integral PlainImage_Integral_NEON
    #define PlainImage_Min PlainImage_Min_NEON
    #define PlainImage_Max PlainImage_Max_NEON
    #define PlainImage_AbsGrayDiff420v PlainImage_AbsGrayDiff420v_NEON
    #define PlainImage_AbsGrayDiff PlainImage_AbsGrayDiff_NEON
#else
    #define PlainImage_RGBFromBGRX8 PlainImage_RGBFromBGRX8_C
    #define PlainImage_420vToRGB PlainImage_420vToRGB_C
    #define PlainImage_Integral PlainImage_Integral_C
    #define PlainImage_Min PlainImage_Min_C
    #define PlainImage_Max PlainImage_Max_C
    #define PlainImage_AbsGrayDiff420v PlainImage_AbsGrayDiff420v_C
    #define PlainImage_AbsGrayDiff PlainImage_AbsGrayDiff_C
#endif

    void PlainImage_Set(PlainImage dst, uint8_t val);

    void PlainImage_RGBDistSquared( PlainImage dest, PlainImage a, PlainImage b );

    void PlainImage_GrayHistogram( PlainImage img, int x, int y, int width, int height, int hist[256]);

    int PlainImage_Average( PlainImage img, const int x1, const int y1, const int x2, const int y2 );

    void PlainImage_Copy(PlainImage dst, PlainImage src);

    void PlainImage_HFlip( PlainImage im );

    void PlainImage_VFlip( PlainImage im );

    PlainImage PlainImage_Retain(PlainImage img);

    void PlainImage_Destroy(PlainImage img);

    int PlainImage_InstanceCount(void);

#ifdef __cplusplus
}
#endif

#endif // PLAIN_IMAGE_H
