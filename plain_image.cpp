#include "plain_image.h"
#ifdef USE_OPENCV_CPP
#include <opencv/highgui.h>
#endif
#include <stdlib.h>
#include <assert.h>
#include <pthread.h>

#define HORKEY_MIN(a, b) (((a) < (b)) ? (a) : (b))
static inline uint8_t clip_u8( int x ) { return x < 0 ? 0 : (x > 255 ? 255 : x); }

#define LOCK(X) do {} while(pthread_mutex_lock(X))
#define UNLOCK(X) pthread_mutex_unlock(X)

// To keep track of how many live instances exist at any point
static int  instance_counter = 0;
static pthread_mutex_t instance_counter_mutex = PTHREAD_MUTEX_INITIALIZER;

static void integral_test();

bool PlainImage_RunTests()
{
    integral_test();

    #ifdef __ARM_NEON__
    extern void PlainImage_RunTests_OSX( void );
    PlainImage_RunTests_OSX();
    #endif

    return true;
}


PlainImage PlainImage_Create(int width, int height, int bytes_per_sample, int channels)
{
    PlainImage img;
    int size_needed, i, stride;

    //Zero-sized images are allowed
    assert(width >= 0);
    assert(height >= 0);

    // The actual struct
    size_needed = sizeof(*img);

    // Pointers to rows
    size_needed += sizeof(img->pixels[0]) * height;

    // Pixel data
    size_needed += width * height * bytes_per_sample;

    img = (PlainImage) malloc(size_needed);

    img->buffer = NULL;
    img->width = width;
    img->height = height;
    img->bytes_per_sample = bytes_per_sample;
    img->channels = channels;
    img->continuous = true;
    img->is_420v = false;
    img->_refcount  = 1;

    // Init row pointers
    img->pixels = (uint8_t **)(img + 1);
    img->pixels[0] = (uint8_t *) (img->pixels + height);
    stride = width * bytes_per_sample;
    for (i = 1; i < height; ++i)
    {
        img->pixels[i] = img->pixels[i-1] + stride;
    }

    LOCK(&instance_counter_mutex);

    instance_counter += 1;

    UNLOCK(&instance_counter_mutex);

    return img;
}

PlainImage PlainImage_Create420v(int width, int height)
{
    assert( (width & 1) == 0 );
    assert( (height & 1) == 0 );

    int cheight = height>>1;
    PlainImage img = PlainImage_Create( width, height+cheight, 1, 1);

    //Hide chroma rows
    img->height = height;
    img->is_420v = true;

    return img;
}

PlainImage PlainImage_CreateFrom420v( int width, int height, const uint8_t *luma, int luma_stride, const uint8_t *chroma, int chroma_stride )
{
    PlainImage im = PlainImage_Create420v( width, height );

    if( im->continuous && luma_stride==width )
    {
        memcpy( im->pixels[0], luma, width*height );
    }
    else
    {
        for( int y = 0; y < height; y++ )
        {
            memcpy( im->pixels[y], luma, width );
            luma += luma_stride;
        }
    }

    if( im->continuous && chroma_stride==width )
    {
        memcpy( im->pixels[height], chroma, width*(height>>1) );
    }
    else
    {
        for( int y = 0; y < height>>1; y++ )
        {
            memcpy( im->pixels[height+y], chroma, width );
            chroma += chroma_stride;
        }
    }

    return im;
}



PlainImage PlainImage_Resize(PlainImage img, int width, int height)
{
    //Caller must completely own this image
    assert( img->_refcount == 1 );

    //Don't realloc on shrink
    if( width<=img->width && height<=img->height )
    {
        if( width != img->width )
            img->continuous = false;

        img->width = width;
        img->height = height;
        return img;
    }


    PlainImage newimg = PlainImage_Create( width, height, img->bytes_per_sample, img->channels );
    memset( newimg->pixels[0], 0, newimg->width*newimg->height*newimg->bytes_per_sample );

    int copywidth = (width < img->width) ? width : img->width;
    int copyheight = (height < img->height) ? height : img->height;
    int copybytes = copywidth * img->bytes_per_sample;

    for( int y = 0; y < copyheight; y++ )
        memcpy( newimg->pixels[y], img->pixels[y], copybytes );

    PlainImage_Destroy( img );
    return newimg;
}


void PlainImage_CopyRect( PlainImage src, int src_x, int src_y, PlainImage dst, int dst_x, int dst_y, int width, int height )
{
    assert( src->bytes_per_sample==dst->bytes_per_sample );
    assert( src->channels==dst->channels );
    int bpp = src->bytes_per_sample;

    if( src_x < 0 )
    {
        dst_x -= src_x;
        width += src_x;
        src_x = 0;
    }
    if( src_y < 0 )
    {
        dst_y -= src_y;
        height += src_y;
        src_y = 0;
    }

    if( dst_x < 0 )
    {
        src_x -= dst_x;
        width += dst_x;
        dst_x = 0;
    }
    if( dst_y < 0 )
    {
        src_y -= dst_y;
        height += dst_y;
        dst_y = 0;
    }

    if( src_x+width > src->width )      width = src->width-src_x;
    if( src_y+height > src->height )    height = src->height-src_y;
    if( dst_x+width > dst->width )      width = dst->width-dst_x;
    if( dst_y+height > dst->height )    height = dst->height-dst_y;

    if( width <= 0 || height <= 0 )
        return;

    for( int y = 0; y < height; y++ )
    {
        uint8_t *srcpix = src->pixels[y+src_y] + src_x*bpp;
        uint8_t *dstpix = dst->pixels[y+dst_y] + dst_x*bpp;
        memcpy( dstpix, srcpix, width*bpp );
    }
}

//Specifically for ingesting Apple's preferred video frame format
void PlainImage_RGBToBGR(PlainImage img)
{
    int r, c;
    int width = img->width;
    int height = img->height;

    if (img->continuous)
    {
        width = width * height;
        height = 1;
    }

    for (r = 0; r < height; r++)
    {
        uint8_t *row    = img->pixels[r];

        for (c = 0; c < width; c++)
        {
            uint8_t tmp = row[3*c];
            row[3*c] = row[3*c + 2];
            row[3*c + 2] = tmp;
        }
    }
}


void PlainImage_RGBMergeA(PlainImage dest, PlainImage rgb, PlainImage a)
{
    assert( rgb->width==a->width && rgb->width==dest->width);
    assert( rgb->height==a->height && rgb->height==dest->height);
    assert( rgb->channels==3 && a->channels==1 && dest->channels==4);
    assert( rgb->bytes_per_sample==3 && a->bytes_per_sample==1 && dest->bytes_per_sample==4);

    int width = rgb->width;
    int height = rgb->height;
    if( rgb->continuous && a->continuous && dest->continuous )
    {
        width *= height;
        height = 1;
    }

    for( int y = 0; y < height; y++ )
    {
        const uint8_t * rgb_row =       rgb->pixels[y];
        const uint8_t * a_row =         a->pixels[y];
        uint8_t *       dest_row =      dest->pixels[y];

        for( int x = 0; x < width; x++ )
        {
            *dest_row++ = *rgb_row++;
            *dest_row++ = *rgb_row++;
            *dest_row++ = *rgb_row++;
            *dest_row++ = *a_row++;
        }
    }
}


//Specifically for ingesting Apple's preferred video frame format
PlainImage PlainImage_RGBFromBGRX8_C(int width, int height, const uint8_t *src)
{
    PlainImage im = PlainImage_Create(width, height, 3, 3);
    assert( im->continuous );

    uint8_t *dest = im->pixels[0];
    for( int count=width*height; count>=0; count-- )
    {
        dest[0] = src[2];
        dest[1] = src[1];
        dest[2] = src[0];
        dest += 3;
        src += 4;
    }
    return im;
}

PlainImage PlainImage_420vToRGB_C( PlainImage yuv )
{
    assert( yuv->is_420v );

    int width = yuv->width;
    int height = yuv->height;
    PlainImage rgb = PlainImage_Create(yuv->width, yuv->height, 3, 3);

    for( int y = 0; y < height; y++ )
    {
        uint8_t *dest = rgb->pixels[y];
        uint8_t *luma = yuv->pixels[y];
        uint8_t *chroma = yuv->pixels[height+(y>>1)];

        for( int x = 0; x < width; x+=2 )
        {
            int y1 = *luma++;
            int y2 = *luma++;
            int cb = *chroma++;
            int cr = *chroma++;

            *dest++ = clip_u8( (74*y1          + 102*cr - 14234) >> 6 );
            *dest++ = clip_u8( (74*y1 -  25*cb -  52*cr +  8711) >> 6 );
            *dest++ = clip_u8( (74*y1 + 129*cb          - 17683) >> 6 );

            *dest++ = clip_u8( (74*y2          + 102*cr - 14234) >> 6 );
            *dest++ = clip_u8( (74*y2 -  25*cb -  52*cr +  8711) >> 6 );
            *dest++ = clip_u8( (74*y2 + 129*cb          - 17683) >> 6 );        }
    }

    return rgb;
}


PlainImage PlainImage_RGBTo420v(PlainImage rgb)
{
    assert( rgb->channels==3 && rgb->bytes_per_sample==3 );

    int width = rgb->width;
    int height = rgb->height;
    PlainImage yuv = PlainImage_Create420v(width, height);

    for( int y = 0; y < height; y += 2 )
    {
        const uint8_t *src1 = rgb->pixels[y];
        const uint8_t *src2 = rgb->pixels[y+1];

        uint8_t *luma1 = yuv->pixels[y];
        uint8_t *luma2 = yuv->pixels[y+1];
        uint8_t *chroma = yuv->pixels[height+(y>>1)];

        for( int x = 0; x < width; x += 2 )
        {
            int r1 = *src1++;   int g1 = *src1++;   int b1 = *src1++;
            int r2 = *src1++;   int g2 = *src1++;   int b2 = *src1++;
            int r3 = *src2++;   int g3 = *src2++;   int b3 = *src2++;
            int r4 = *src2++;   int g4 = *src2++;   int b4 = *src2++;

            //Y = (66*R + 129*G + 25*B + 128)/256 + 16
            *luma1++ = clip_u8( ((66*r1 + 129*g1 + 25*b1 + 128) >> 8) + 16 );
            *luma1++ = clip_u8( ((66*r2 + 129*g2 + 25*b2 + 128) >> 8) + 16 );
            *luma2++ = clip_u8( ((66*r3 + 129*g3 + 25*b3 + 128) >> 8) + 16 );
            *luma2++ = clip_u8( ((66*r4 + 129*g4 + 25*b4 + 128) >> 8) + 16 );


            int ra = (r1+r2+r3+r4+2)>>2;
            int ga = (g1+g2+g3+g4+2)>>2;
            int ba = (b1+b2+b3+b4+2)>>2;

            //Cb = -38*R - 74*G + 112*B + 128)/256 + 128
            //Cr = 112*R - 94*G - 18*B + 128)/256 + 128
            *chroma++ = clip_u8( ((-38*ra - 74*ga + 112*ba + 128)>>8) + 128);
            *chroma++ = clip_u8( ((112*ra - 94*ga - 18*ba + 128)>>8) + 128);
        }
    }

    return yuv;
}



#ifdef USE_OPENCV_CPP

bool PlainImage_ShallowCopyIplImage(PlainImage *pdst, IplImage *src)
{
    CvMat *mat, mat_header;
    int channels = src->nChannels;
    int bytes_per_sample = 1, i;
    PlainImage dst = *pdst;

    mat = cvGetMat(src, &mat_header);

    switch(src->depth)
    {
    case IPL_DEPTH_8U:
        bytes_per_sample = 1;
        break;

    case IPL_DEPTH_32S:
        bytes_per_sample = 4;
        break;

    default:
        assert(!"Unknown Mat type!");
        break;
    }

    bytes_per_sample *= channels;

    if (dst == NULL ||
        dst->height != src->height ||
        dst->width  != src->width )
    {
        PlainImage_Destroy(dst);
        // 0 bytes per sample so that no actual space for pixes is allocated
        *pdst = dst = PlainImage_Create(src->width, src->height, 0, channels);
    }

    dst->bytes_per_sample = bytes_per_sample;
    dst->channels = channels;
    dst->continuous =  true;

    for (i = 0; i < dst->height; i++)
    {
        dst->pixels[i] = CV_MAT_ELEM_PTR(*mat, i, 0);
    }

    return true;
}

bool PlainImage_DeepCopyIplImage(PlainImage *pdst, IplImage *src)
{
    CvMat *mat, mat_header;
    int channels = src->nChannels;
    int bytes_per_sample = 1, i;
    PlainImage dst = *pdst;

    mat = cvGetMat(src, &mat_header);

    switch(src->depth)
    {
    case IPL_DEPTH_8U:
        bytes_per_sample = 1;
        break;

    case IPL_DEPTH_32S:
        bytes_per_sample = 4;
        break;

    default:
        assert(!"Unknown Mat type!");
        break;
    }

    bytes_per_sample *= channels;

    if (dst == NULL ||
        dst->height != src->height ||
        dst->width  != src->width ||
        dst->channels != channels ||
        dst->bytes_per_sample != bytes_per_sample)
    {
        PlainImage_Destroy(dst);
        *pdst = dst = PlainImage_Create(src->width, src->height, bytes_per_sample, channels);
    }

    dst->bytes_per_sample = bytes_per_sample;
    dst->channels = channels;
    dst->continuous =  true;

    for (i = 0; i < dst->height; i++)
    {
        memcpy(dst->pixels[i], CV_MAT_ELEM_PTR(*mat, i, 0), dst->width * bytes_per_sample);
    }

    return true;
}

PlainImage PlainImage_LoadPng_OCV(const char *filename)
{
    IplImage *ocv_img = NULL;
    PlainImage img    = NULL;

    assert(filename);

    if (!(ocv_img = cvLoadImage(filename, CV_LOAD_IMAGE_UNCHANGED)))
    {
        return NULL;
    }

    if (!PlainImage_DeepCopyIplImage(&img, ocv_img))
    {
        img = NULL;
    }

    cvReleaseImage(&ocv_img);

    return img;
}



bool PlainImage_SavePng(const PlainImage img, const char *filename)
{
    bool result = false;

    if( img->is_420v )
    {
        PlainImage rgb = PlainImage_420vToRGB( img );
        result = PlainImage_SavePng( rgb, filename );
        PlainImage_Destroy( rgb );
        return result;
    }

    IplImage *ocv_img;

    assert(img);
    assert(img->bytes_per_sample == img->channels);

    ocv_img = PlainImage_ToIplImage(img);

    if (ocv_img)
    {
        result = cvSaveImage(filename, ocv_img);
        cvReleaseImage(&ocv_img);
    }

    return result;
}

IplImage *PlainImage_ToIplImage(const PlainImage img)
{
    IplImage *ocv_img;

    assert(img);
    assert(img->bytes_per_sample == img->channels);

    int row_bytes = img->width * img->bytes_per_sample;
    ocv_img = cvCreateImage(cvSize(img->width, img->height), IPL_DEPTH_8U, img->channels);

    if (ocv_img)
    {
        memcpy(ocv_img->imageData, img->pixels[0], row_bytes * img->height);
    }

    return ocv_img;
}

#elif defined(NO_PNG_OUTPUT)

bool PlainImage_SavePng(const PlainImage img, const char *filename)
{
    return false;
}

#endif


void PlainImage_Min_C(PlainImage dest, PlainImage src1, PlainImage src2)
{
    int width, height, r, c;

    assert(src1->width == src2->width);
    assert(src1->height == src2->height);
    assert(src1->channels == src2->channels);
    assert(src1->width == dest->width);
    assert(src1->height == dest->height);
    assert(src1->channels == dest->channels);
    assert(src1->bytes_per_sample == src1->channels);
    assert(src2->bytes_per_sample == src2->channels);
    assert(dest->bytes_per_sample == dest->channels);

    width = src1->width * src1->channels;
    height = src1->height;

    if (dest->continuous && src1->continuous && src2->continuous)
    {
        width *= height;
        height = 1;
    }

    for (r = 0; r < height; r++)
    {
        uint8_t *row_dst = dest->pixels[r];
        uint8_t *row1    = src1->pixels[r];
        uint8_t *row2    = src2->pixels[r];

        for (c = 0; c < width; c++)
        {
            *(row_dst++) = (row1[0] < row2[0]) ? row1[0] : row2[0];
            row1++;
            row2++;
        }
    }

}

void PlainImage_Max_C(PlainImage dest, PlainImage src1, PlainImage src2)
{
    int width, height, r, c;

    assert(src1->width == src2->width);
    assert(src1->height == src2->height);
    assert(src1->channels == src2->channels);
    assert(src1->width == dest->width);
    assert(src1->height == dest->height);
    assert(src1->channels == dest->channels);
    assert(src1->bytes_per_sample == src1->channels);
    assert(src2->bytes_per_sample == src2->channels);
    assert(dest->bytes_per_sample == dest->channels);

    width = src1->width * src1->channels;
    height = src1->height;

    if (dest->continuous && src1->continuous && src2->continuous)
    {
        width *= height;
        height = 1;
    }

    for (r = 0; r < height; r++)
    {
        uint8_t *row_dst = dest->pixels[r];
        uint8_t *row1    = src1->pixels[r];
        uint8_t *row2    = src2->pixels[r];

        for (c = 0; c < width; c++)
        {
            *(row_dst++) = (row1[0] > row2[0]) ? row1[0] : row2[0];
            row1++;
            row2++;
        }
    }

}

void PlainImage_BitwiseAnd(PlainImage img, uint8_t bitmask)
{
    int width, height, r, c;

    assert(img);
    assert(img->bytes_per_sample == img->channels);

    width = img->width * img->channels;
    height = img->height;

    if (img->continuous)
    {
        width *= height;
        height = 1;
    }

    for (r = 0; r < height; r++)
    {
        uint8_t *row = img->pixels[r];

        for (c = 0; c < width; c++)
        {
            row[c] &= bitmask;
        }
    }

}

void PlainImage_Integral_C(PlainImage integral, PlainImage input)
{
    assert(input->width + 1 == integral->width);
    assert(input->height + 1 == integral->height);
    assert(input->bytes_per_sample == 1);
    assert(input->channels == 1);
    assert(integral->bytes_per_sample == 4);
    assert(integral->channels == 1);

    //Init first row to zero
    memset( integral->pixels[0], 0, integral->width*integral->bytes_per_sample );

    for( int y = 1; y < integral->height; y++ )
    {
        const uint32_t *up  = (uint32_t*)integral->pixels[y-1];
              uint32_t *dst = (uint32_t*)integral->pixels[y];
        const uint8_t  *img = input->pixels[y-1];

        //Init first column to zero
        *(dst++) = 0;
        up++;

        uint32_t rowsum = 0;        //Running total sum for this row

        for( int count = integral->width-1; count > 0; count-- )
        {
            rowsum += *(img++);
            *(dst++) = rowsum + *(up++);
        }
    }
}


#define BYTES_PER_SAMPLE 3 // RGB

#define ABSDIFF(X, Y) ( ((X) > (Y)) ? (X)-(Y) : (Y) - (X) )


void PlainImage_Set(PlainImage dst, uint8_t val)
{
    int r;
    int width = dst->width;
    int height = dst->height;

    if (dst->continuous)
    {
        width = width * height;
        height = 1;
    }

    for (r = 0; r < height; r++)
    {
        memset(dst->pixels[r], val, dst->bytes_per_sample * width);
    }
}

void PlainImage_Copy(PlainImage dst, PlainImage src)
{
    int r;
    int width  = dst->width;
    int height = dst->height;

    assert(dst->height == src->height);
    assert(dst->width == src->width);
    assert(dst->bytes_per_sample == src->bytes_per_sample);
    assert(dst->channels == src->channels);

    if (dst->continuous && src->continuous)
    {
        width = width * height;
        height = 1;
    }

    for (r = 0; r < height; r++)
    {
        memcpy(dst->pixels[r], src->pixels[r], dst->bytes_per_sample * width);
    }
}



void PlainImage_AbsGrayDiff420v_C(PlainImage dstim, PlainImage a, PlainImage b)
{
    int width = dstim->width;
    int height = dstim->height;

    for( int y = 0; y < height; y++ )
    {
        const uint8_t *aluma = a->pixels[y];
        const uint8_t *bluma = b->pixels[y];
        const uint8_t *achroma = a->pixels[height+(y>>1)];
        const uint8_t *bchroma = b->pixels[height+(y>>1)];
        uint8_t *dest = dstim->pixels[y];

        for( int x = 0; x < width; x += 2 )
        {
            int al1 = *aluma++;     int al2 = *aluma++;
            int bl1 = *bluma++;     int bl2 = *bluma++;
            int acb = *achroma++;   int acr = *achroma++;
            int bcb = *bchroma++;   int bcr = *bchroma++;

            int lumdiff1 = ABSDIFF(al1, bl1);
            int lumdiff2 = ABSDIFF(al2, bl2);
            int cdiff = (ABSDIFF(acb, bcb) + ABSDIFF(acr, bcr)) >> 1;
            int delta1 = (lumdiff1 << 1) + cdiff;
            int delta2 = (lumdiff2 << 1) + cdiff;
            *dest++ = HORKEY_MIN(delta1, 255);
            *dest++ = HORKEY_MIN(delta2, 255);
        }
    }
}

void PlainImage_AbsGrayDiff_C(PlainImage dst, PlainImage a, PlainImage b)
{
    int r, c, width, height;
    uint8_t *ptr_a, *ptr_b, *ptr_dst;

    width  = dst->width;
    height = dst->height;

    assert(a->width == width);
    assert(a->height == height);
    assert(b->width == width);
    assert(b->height == height);

    if( a->is_420v && b->is_420v )
    {
        PlainImage_AbsGrayDiff420v( dst, a, b );
        return;
    }

    assert(a->bytes_per_sample == BYTES_PER_SAMPLE && b->bytes_per_sample == BYTES_PER_SAMPLE);
    assert(a->channels == BYTES_PER_SAMPLE && b->channels == BYTES_PER_SAMPLE);
    assert(dst->channels == 1 && dst->bytes_per_sample == 1);

    // If this is the case, treat all files as a continuous buffer
    if (a->continuous && b->continuous && dst->continuous)
    {
        width = width * height;
        height = 1;
    }

    for (r = 0; r < height; r++)
    {

        ptr_a = a->pixels[r];
        ptr_b = b->pixels[r];
        ptr_dst = dst->pixels[r];

        for (c = 0; c < width; c++)
        {
            int d1 = ABSDIFF(ptr_a[0], ptr_b[0]);
            int d2 = ABSDIFF(ptr_a[1], ptr_b[1]);
            int d3 = ABSDIFF(ptr_a[2], ptr_b[2]);
            int total_delta = (d1 + d2 + d3) >> 1;
            *(ptr_dst++) = HORKEY_MIN(total_delta, 255);
            ptr_a += BYTES_PER_SAMPLE;
            ptr_b += BYTES_PER_SAMPLE;
        }
    }


}

PlainImage PlainImage_Retain(PlainImage img)
{
    assert(img);
    assert(img->_refcount > 0);

    img->_refcount += 1;
    return img;
}


void PlainImage_Destroy(PlainImage img)
{
    // Allow passing a NULL image to simplify cleanup code.
    if (img == NULL)
    {
        return;
    }

    assert(img->_refcount > 0);
    img->_refcount -= 1;

    if (img->_refcount == 0)
    {
        // Decrease the instances counter
        LOCK(&instance_counter_mutex);

        assert(instance_counter > 0);
        instance_counter -= 1;

        UNLOCK(&instance_counter_mutex);

        // Actually release the image and image buffer
        if (img->buffer)
        {
            free(img->buffer);
        }

        free(img);
    }
}


static void integral_test()
{
    int r, c;
    PlainImage src = PlainImage_Create(4, 3, 1, 1);
    PlainImage integral = PlainImage_Create(src->width + 1, src->height + 1, 4, 1);

    int32_t result[4][5] = { {0, 0,   0,  0, 0},
        {0, 0,   1,  3, 6},
        {0, 10, 22, 36, 52},
        {0, 30, 63, 99, 138}
    };

    // Construct a synthetic table
    for (r = 0; r < src->height; r++)
    {
        for (c = 0; c < src->width; c++)
        {
            src->pixels[r][c] = 10 * r + c;
        }
    }

    PlainImage_Integral_C(integral, src);

    for (r = 0; r < integral->height; r++)
    {
        for (c = 0; c < integral->width; c++)
        {
            int *integ = (int *) integral->pixels[0];
            int sum = integ[integral->width * r + c];
            assert(sum == result[r][c]);
        }
    }

    PlainImage_Destroy(integral);
    PlainImage_Destroy(src);
}



void PlainImage_RGBDistSquared( PlainImage dest, PlainImage a, PlainImage b )
{
    //Redundant (see AbsGreyDiff) but kept for the moment until new code tested with old ball-detector
    assert( a->width==b->width && a->height==b->height );
    assert( a->channels==3 && a->bytes_per_sample==3 && b->channels==3 && b->bytes_per_sample==3 );

    for( int y = 0; y < dest->height; y++ )
    {
        const uint8_t *apix = a->pixels[y];
        const uint8_t *bpix = b->pixels[y];
        uint8_t *rpix = dest->pixels[y];

        for( int x=dest->width; x>=0; x-- )
        {
            int db = *(apix++) - *(bpix++);
            int dg = *(apix++) - *(bpix++);
            int dr = *(apix++) - *(bpix++);

            int d = (dr*dr + dg*dg + db*db) / 128;

            *(rpix++) = d>255 ? 255 : d;
        }
    }
}

/* Computes the average value within an image area */
int PlainImage_Average( PlainImage img, const int x1, const int y1, const int x2, const int y2 )
{
    assert(img);
    assert(img->channels == img->bytes_per_sample);
    assert(x1 >= 0 && x1 < x2);
    assert(y1 >= 0 && y1 < y2);
    assert(x2 < img->width);
    assert(y1 < img->height);

    uint32_t sum = 0;

    const int x_max = (x2 + 1 - x1) * img->bytes_per_sample;
    const int count = (x_max - x1) * (y2 - y1 + 1);

    for (int y = y1; y <= y2; y++)
    {

        uint8_t *row = img->pixels[y];
        for (int x = x1; x < x_max; x++)
        {
            sum += row[x];
        }
    }

    return sum / count;
}


void PlainImage_GrayHistogram( PlainImage img, int x, int y, int width, int height, int hist[256])
{
    assert(img);
    assert(img->channels == 1);
    assert(img->bytes_per_sample == 1);
    assert(x + width <= img->width);
    assert(y + height <= img->height);

    memset(hist, 0, 255 * sizeof(hist[0]));
    const int y_end = y + height;
    const int x_end = x + width;

    for (int r = y; r < y_end; r++)
    {
        uint8_t *row = img->pixels[r];
        for (int c = x; c < x_end; c++)
        {
            hist[row[c]] += 1;
        }
    }
}


void PlainImage_HFlip( PlainImage im )
{
    assert( im->bytes_per_sample==im->channels );

    for( int y = 0; y < im->height; y++ )
    {
        uint8_t *start = &im->pixels[y][0];
        uint8_t *end = &im->pixels[y][(im->width-1)*im->channels];
        do
        {
            for( int c = 0; c < im->channels; c++ )
            {
                uint8_t t = start[c];
                start[c] = end[c];
                end[c] = t;
            }
            start += im->channels;
            end -= im->channels;
        }
        while( start < end );
    }
}

void PlainImage_VFlip( PlainImage im )
{
    size_t rowlen = im->width * im->bytes_per_sample;
    uint8_t *tmp = new uint8_t [rowlen];

    for( int y = 0; y < im->height/2; y++ )
    {
        uint8_t *row1 = im->pixels[y];
        uint8_t *row2 = im->pixels[im->height-y-1];
        memcpy( tmp, row1, rowlen );
        memcpy( row1, row2, rowlen );
        memcpy( row2, tmp, rowlen );
    }

    delete [] tmp;
}

bool PlainImage_Rgb2Grayscale( PlainImage im )
{
    assert(im);

    if (im->bytes_per_sample != im->channels || im->channels != 3)
        return false;
    int width  = im->width;
    int height = im->height;

    if (im->continuous)
    {
        width *= height;
        height = 1;
    }

    for( int y = 0; y < height; y++ )
    {
        uint8_t *row = im->pixels[y];
        for (int x = 0, c = 0; x < width; x++, c+=3)
        {
            row[x] = (int(11*row[c]) + int(16*row[c+1]) + int(5*row[c+2]))/32;
        }
    }

    im->bytes_per_sample = im->channels = 1;

    // Repair row pointers, which will be broken for continuous images
    for (int i = 1; i < im->height && im->continuous; i++)
    {
        im->pixels[i] = im->pixels[i-1] + im->width;
    }

    return true;
}

int PlainImage_InstanceCount(void)
{
    int count;

    // Decrease the instances counter
    LOCK(&instance_counter_mutex);

    count = instance_counter;

    UNLOCK(&instance_counter_mutex);

    assert(count >= 0);

    return count;
}
