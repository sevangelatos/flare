// 2D phase unwrapping, modified for inclusion in scipy by Gregor Thalhammer

//This program was written by Munther Gdeisat and Miguel Arevallilo Herraez to program the two-dimensional unwrapper
//entitled "Fast two-dimensional phase-unwrapping algorithm based on sorting by
//reliability following a noncontinuous path"
//by  Miguel Arevallilo Herraez, David R. Burton, Michael J. Lalor, and Munther A. Gdeisat
//published in the Journal Applied Optics, Vol. 41, No. 35, pp. 7437, 2002.
//This program was written by Munther Gdeisat, Liverpool John Moores University, United Kingdom.
//Date 26th August 2007
//The wrapped phase map is assumed to be of floating point data type. The resultant unwrapped phase map is also of floating point type.
//The mask is of byte data type.
//When the mask is 255 this means that the pixel is valid
//When the mask is 0 this means that the pixel is invalid (noisy or corrupted pixel)
//This program takes into consideration the image wrap around problem encountered in MRI imaging.

#include "fringeprojection.h"
#include <cassert>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <float.h>
#include <algorithm>

static const float TWOPI = 2*M_PI;

#define NOMASK 255
#define MASK 0

using namespace FringeProjection;

//PIXELM information
struct PIXELM
{
    int increment;		//No. of 2*pi to add to the pixel to unwrap it
    int number_of_pixels_in_group;//No. of pixel in the pixel group
    float value;			//value of the pixel
    float reliability;
    struct PIXELM *head;		//pointer to the first pixel in the group in the linked list
    struct PIXELM *last;		//pointer to the last pixel in the group
    struct PIXELM *next;		//pointer to the next pixel in the group
    uint8_t input_mask;	        //0 pixel is masked. NOMASK pixel is not masked
    uint8_t extended_mask;      //0 pixel is masked. NOMASK pixel is not masked
};

typedef struct PIXELM PIXELM;

//the EDGE is the line that connects two pixels.
//if we have S pixels, then we have S horizontal edges and S vertical edges
struct EDGE
{
    float reliab;			//reliabilty of the edge and it depends on the two pixels
    PIXELM *pointer_1;		//pointer to the first pixel
    PIXELM *pointer_2;		//pointer to the second pixel
    int increment;		//No. of 2*pi to add to one of the pixels to
    //unwrap it with respect to the second
    bool operator < (const EDGE &e) const { return reliab < e.reliab; }
};

inline uint8_t maskOf(const PhasePixel *px, uint8_t min_confidence)
{
    return (px->confidence >= min_confidence) ? NOMASK : MASK;
}

//--------------------start initialize pixels ----------------------------------
//initialize pixels. See the explanation of the pixel class above.
//initially every pixel is assumed to belong to a group consisting of only itself
void  initialisePIXELs(const PlainImage wrapped_image, uint8_t min_confidence, uint8_t *extended_mask, PIXELM *pixel)
{
    PIXELM *pixel_pointer = pixel;
    uint8_t *extended_mask_pointer = extended_mask;

    assert(pixel);
    assert(wrapped_image);
    int width = wrapped_image->width;
    int height = wrapped_image->height;

    if (wrapped_image->continuous)
    {
        width  = width * height;
        height = 1;
    }

    for (int r=0; r < height; r++)
    {
        const PhasePixel *row = (const PhasePixel*)(wrapped_image->pixels[r]);
        for (int c = 0; c < width; c++)
        {
            pixel_pointer->increment = 0;
            pixel_pointer->number_of_pixels_in_group = 1;
            pixel_pointer->value = row[c].phase;
            pixel_pointer->reliability = 9999999.0f + rand();
            pixel_pointer->input_mask = maskOf(row+c, min_confidence);
            pixel_pointer->extended_mask = *extended_mask_pointer;
            pixel_pointer->head = pixel_pointer;
            pixel_pointer->last = pixel_pointer;
            pixel_pointer->next = NULL;
            pixel_pointer++;
            extended_mask_pointer++;
        }
    }

}
//-------------------end initialize pixels -----------

//gamma function in the paper
inline float wrap(float pixel_value)
{
    if (pixel_value > M_PI)
        return pixel_value - TWOPI;
    else if (pixel_value < -M_PI)
        return pixel_value + TWOPI;
    else
        return pixel_value;
}

// pixelL_value is the left pixel,	pixelR_value is the right pixel
int find_wrap(float pixelL_value, float pixelR_value)
{
    float difference = pixelL_value - pixelR_value;
    return (difference > M_PI) ? -1 : ( (difference < -M_PI) ? 1 : 0 );
}

inline bool unmasked(const PhasePixel *px, uint8_t min_confidence)
{
    return px->confidence >= min_confidence;
}

uint8_t *create_extended_mask(const PlainImage image, uint8_t t)
{
    assert(image);
    int width = image->width;
    int height = image->height;
    uint8_t *extended_mask = (uint8_t *) calloc(width*height, sizeof(uint8_t));
    uint8_t *EMP = extended_mask + width + 1;	//extended mask pointer

    //extend the mask for the image except borders
    for (int r=1; r < height - 1; ++r)
    {
        const PhasePixel *row_n = (const PhasePixel*)(image->pixels[r-1]);
        const PhasePixel *row   = (const PhasePixel*)(image->pixels[r]);
        const PhasePixel *row_s = (const PhasePixel*)(image->pixels[r+1]);

        for (int c=1; c < width - 1; ++c)
        {
            if ( unmasked(row_n+c, t) && unmasked(row_n+c+1, t) && unmasked(row_n+c-1, t) &&
                 unmasked(row+c, t) && unmasked(row+c+1, t) && unmasked(row+c-1, t) &&
                 unmasked(row_s+c, t) && unmasked(row_s+c+1, t) && unmasked(row_s+c-1, t) )
            {
                *EMP = NOMASK;
            }
            ++EMP;
        }
        EMP += 2;
    }

    return extended_mask;
}

void calculate_reliability(PIXELM *pixel, int width, int height)
{
    PIXELM *px = pixel + width+1;

    for (int i = 1; i < height-1; ++i)
    {
        for (int j = 1; j < width-1; ++j)
        {
            if (px->extended_mask == NOMASK)
            {
                const PIXELM *px_n = px - width;
                const PIXELM *px_s = px + width;
                const float nw = px_n[-1].value;
                const float n  = px_n[0].value;
                const float ne = px_n[+1].value;
                const float w  = px[-1].value;
                const float c  = px[0].value;
                const float e  = px[+1].value;
                const float sw = px_s[-1].value;
                const float s  = px_s[0].value;
                const float se = px_s[+1].value;
                const float H = wrap(w - c) - wrap(c - e);
                const float V = wrap(n - c) - wrap(c - s);
                const float D1 = wrap(nw - c) - wrap(c - se);
                const float D2 = wrap(ne - c) - wrap(c - sw);
                px->reliability = H*H + V*V + D1*D1 + D2*D2;
            }
            px++;
        }
        px += 2;
    }

}

//calculate the reliability of the horizontal edges of the image
//it is calculated by adding the reliability of pixel and the relibility of
//its right-hand neighbour
//edge is calculated between a pixel and its next neighbour
void  horizontalEDGEs(PIXELM *pixel, EDGE *edge,
                      int image_width, int image_height,
                      int *num_edges)
{
    int i, j;
    EDGE *edge_pointer = edge;
    PIXELM *pixel_pointer = pixel;
    int no_of_edges = *num_edges;

    for (i = 0; i < image_height; i++)
    {
        for (j = 0; j < image_width - 1; j++)
        {
            if (pixel_pointer->input_mask == NOMASK && (pixel_pointer + 1)->input_mask == NOMASK)
            {
                edge_pointer->pointer_1 = pixel_pointer;
                edge_pointer->pointer_2 = (pixel_pointer+1);
                edge_pointer->reliab = pixel_pointer->reliability + (pixel_pointer + 1)->reliability;
                edge_pointer->increment = find_wrap(pixel_pointer->value, (pixel_pointer + 1)->value);
                edge_pointer++;
                no_of_edges++;
            }
            pixel_pointer++;
        }
        pixel_pointer++;
    }
    *num_edges = no_of_edges;
}

//calculate the reliability of the vertical edges of the image
//it is calculated by adding the reliability of pixel and the relibility of
//its lower neighbour in the image.
void  verticalEDGEs(PIXELM *pixel, EDGE *edge,
                    int image_width, int image_height,
                    int *num_edges)
{
    int i, j;
    int no_of_edges = (*num_edges);
    PIXELM *pixel_pointer = pixel;
    EDGE *edge_pointer = edge + (*num_edges);

    for (i=0; i < image_height - 1; i++)
    {
        for (j=0; j < image_width; j++)
        {
            if (pixel_pointer->input_mask == NOMASK && (pixel_pointer + image_width)->input_mask == NOMASK)
            {
                edge_pointer->pointer_1 = pixel_pointer;
                edge_pointer->pointer_2 = (pixel_pointer + image_width);
                edge_pointer->reliab = pixel_pointer->reliability + (pixel_pointer + image_width)->reliability;
                edge_pointer->increment = find_wrap(pixel_pointer->value, (pixel_pointer + image_width)->value);
                edge_pointer++;
                no_of_edges++;
            }
            pixel_pointer++;
        } //j loop
    } // i loop

    (*num_edges) = no_of_edges;
}

//gather the pixels of the image into groups
void  gatherPIXELs(EDGE *edge, int num_edges)
{
    int k;
    PIXELM *PIXEL1;
    PIXELM *PIXEL2;
    PIXELM *group1;
    PIXELM *group2;
    EDGE *pointer_edge = edge;
    int incremento;

    for (k = 0; k < num_edges; k++)
    {
        PIXEL1 = pointer_edge->pointer_1;
        PIXEL2 = pointer_edge->pointer_2;

        //PIXELM 1 and PIXELM 2 belong to different groups
        //initially each pixel is a group by it self and one pixel can construct a group
        //no else or else if to this if
        if (PIXEL2->head != PIXEL1->head)
        {
            //PIXELM 2 is alone in its group
            //merge this pixel with PIXELM 1 group and find the number of 2 pi to add
            //to or subtract to unwrap it
            if ((PIXEL2->next == NULL) && (PIXEL2->head == PIXEL2))
            {
                PIXEL1->head->last->next = PIXEL2;
                PIXEL1->head->last = PIXEL2;
                (PIXEL1->head->number_of_pixels_in_group)++;
                PIXEL2->head=PIXEL1->head;
                PIXEL2->increment = PIXEL1->increment-pointer_edge->increment;
            }

            //PIXELM 1 is alone in its group
            //merge this pixel with PIXELM 2 group and find the number of 2 pi to add
            //to or subtract to unwrap it
            else if ((PIXEL1->next == NULL) && (PIXEL1->head == PIXEL1))
            {
                PIXEL2->head->last->next = PIXEL1;
                PIXEL2->head->last = PIXEL1;
                (PIXEL2->head->number_of_pixels_in_group)++;
                PIXEL1->head = PIXEL2->head;
                PIXEL1->increment = PIXEL2->increment+pointer_edge->increment;
            }

            //PIXELM 1 and PIXELM 2 both have groups
            else
            {
                group1 = PIXEL1->head;
                group2 = PIXEL2->head;
                //if the no. of pixels in PIXELM 1 group is larger than the
                //no. of pixels in PIXELM 2 group.  Merge PIXELM 2 group to
                //PIXELM 1 group and find the number of wraps between PIXELM 2
                //group and PIXELM 1 group to unwrap PIXELM 2 group with respect
                //to PIXELM 1 group.  the no. of wraps will be added to PIXELM 2
                //group in the future
                if (group1->number_of_pixels_in_group > group2->number_of_pixels_in_group)
                {
                    //merge PIXELM 2 with PIXELM 1 group
                    group1->last->next = group2;
                    group1->last = group2->last;
                    group1->number_of_pixels_in_group = group1->number_of_pixels_in_group + group2->number_of_pixels_in_group;
                    incremento = PIXEL1->increment-pointer_edge->increment - PIXEL2->increment;
                    //merge the other pixels in PIXELM 2 group to PIXELM 1 group
                    while (group2 != NULL)
                    {
                        group2->head = group1;
                        group2->increment += incremento;
                        group2 = group2->next;
                    }
                }

                //if the no. of pixels in PIXELM 2 group is larger than the
                //no. of pixels in PIXELM 1 group.  Merge PIXELM 1 group to
                //PIXELM 2 group and find the number of wraps between PIXELM 2
                //group and PIXELM 1 group to unwrap PIXELM 1 group with respect
                //to PIXELM 2 group.  the no. of wraps will be added to PIXELM 1
                //group in the future
                else
                {
                    //merge PIXELM 1 with PIXELM 2 group
                    group2->last->next = group1;
                    group2->last = group1->last;
                    group2->number_of_pixels_in_group = group2->number_of_pixels_in_group + group1->number_of_pixels_in_group;
                    incremento = PIXEL2->increment + pointer_edge->increment - PIXEL1->increment;
                    //merge the other pixels in PIXELM 2 group to PIXELM 1 group
                    while (group1 != NULL)
                    {
                        group1->head = group2;
                        group1->increment += incremento;
                        group1 = group1->next;
                    } // while

                } // else
            } //else
        } //if
        pointer_edge++;
    }
}

//unwrap the image
void unwrapImage(PIXELM *pixel, int image_width, int image_height, float *minVal)
{
    int i;
    int image_size = image_width * image_height;
    float minUnwrapped = FLT_MAX;
    PIXELM *pixel_pointer=pixel;

    for (i = 0; i < image_size; i++)
    {
        const float val = pixel_pointer->value + TWOPI * (float)(pixel_pointer->increment);
        pixel_pointer->value = val;

        if (pixel_pointer->input_mask == NOMASK) {
            minUnwrapped = std::min(val, minUnwrapped);
        }

        pixel_pointer++;
    }

    *minVal = minUnwrapped;
}

//set the masked pixels (mask = 0) to the minimum of the unwrapper phase
void  maskImage(PIXELM *pixel, uint8_t *input_mask, int image_width, int image_height)
{
    PIXELM *pointer_pixel = pixel;
    uint8_t *IMP = input_mask;	//input mask pointer
    float min=99999999.f;
    int i;
    int image_size = image_width * image_height;

    //find the minimum of the unwrapped phase
    for (i = 0; i < image_size; i++)
    {
        if ((pointer_pixel->value < min) && (*IMP == NOMASK))
            min = pointer_pixel->value;

        pointer_pixel++;
        IMP++;
    }

    pointer_pixel = pixel;
    IMP = input_mask;

    //set the masked pixels to minimum
    for (i = 0; i < image_size; i++)
    {
        if ((*IMP) == MASK)
        {
            pointer_pixel->value = min;
        }
        pointer_pixel++;
        IMP++;
    }
}

//the input to this unwrapper is an array that contains the wrapped
//phase map.  copy the image on the buffer passed to this unwrapper to
//over-write the unwrapped phase map on the buffer of the wrapped
//phase map.
void  returnImage(PIXELM *pixel, PlainImage unwrapped_image, float minUnwrapped)
{
    assert(pixel);
    assert(unwrapped_image);
    int width = unwrapped_image->width;
    int height = unwrapped_image->height;

    if (unwrapped_image->continuous)
    {
        width  = width * height;
        height = 1;
    }

    for (int r=0; r < height; r++)
    {
        PhasePixel *row = (PhasePixel*)(unwrapped_image->pixels[r]);
        for (int c = 0; c < width; c++)
        {
            row[c].phase = (pixel->input_mask == NOMASK) ? pixel->value : minUnwrapped;
            pixel++;
        }
    }
}

//the main function of the unwrapper
void
unwrap2D(PlainImage phase_image, uint8_t min_confidence)
{
    int num_edges = 0;

    PIXELM *pixel;
    EDGE *edge;
    int image_width  = phase_image->width;
    int image_height = phase_image->height;
    int image_size = image_height * image_width;
    int No_of_Edges_initially = 2 * image_width * image_height;

    pixel = (PIXELM *) calloc(image_size, sizeof(PIXELM));
    edge = (EDGE *) calloc(No_of_Edges_initially, sizeof(EDGE));

    uint8_t *extended_mask = create_extended_mask(phase_image, min_confidence);
    initialisePIXELs(phase_image, min_confidence, extended_mask, pixel);

    calculate_reliability(pixel, image_width, image_height);
    horizontalEDGEs(pixel, edge, image_width, image_height, &num_edges);
    verticalEDGEs(pixel, edge, image_width, image_height, &num_edges);

    //sort the EDGEs depending on their reliability. The PIXELs with higher
    //relibility (small value) first
    std::sort(edge, edge+num_edges);

    //gather PIXELs into groups
    gatherPIXELs(edge, num_edges);

    float minUnwrapped;
    unwrapImage(pixel, image_width, image_height, &minUnwrapped);

    returnImage(pixel, phase_image, minUnwrapped);

    free(edge);
    free(pixel);
    free(extended_mask);
}
