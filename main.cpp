#include "fringeprojection.h"
#include <cstdio>
#include <cmath>

#define CHK(X) do { if(!(X)) { \
    fprintf(stderr, "ErrorCheck: %s failed", #X); \
    goto cleanup;} \
    } while(0);

using namespace FringeProjection;
using namespace std;

bool generate_patterns(int width, int height, float period)
{
    bool ok = true;
    PlainImage pattern = PlainImage_Create(width, height, 1, 1);

    GenerateCosinePattern(pattern, period, 0);
    FloydSteinberg(pattern, pattern);
    ok = PlainImage_SavePng(pattern, "pattern_0.png");

    GenerateCosinePattern(pattern, period, 2*M_PI/3);
    FloydSteinberg(pattern, pattern);
    ok = PlainImage_SavePng(pattern, "pattern_1.png") && ok;

    GenerateCosinePattern(pattern, period, -2*M_PI/3);
    FloydSteinberg(pattern, pattern);
    ok = PlainImage_SavePng(pattern, "pattern_2.png") && ok;

    PlainImage_Destroy(pattern);
    return ok;
}

void releaseImages(PlainImage images[], size_t numImages)
{
    for (size_t i = 0; i < numImages; i++) {
        PlainImage_Destroy(images[i]);
        images[i] = NULL;
    }
}

bool loadImages(PlainImage images[], const char* const filenames[], size_t numImages)
{
    bool ok = true;
    // Init pointers to NULL
    memset(images, 0, sizeof(*images)*numImages);

    for (size_t i = 0; i < numImages; i++)
    {
        if ((images[i] = PlainImage_LoadPng(filenames[i])) == NULL) {
            fprintf(stderr, "Could not load %s", filenames[i]);
            ok = false;
        }

        if (images[i]->channels > 1 && !PlainImage_Rgb2Grayscale(images[i])) {
            fprintf(stderr, "Failed to convert %s to grayscale.", filenames[i]);
        }
    }

    return ok;
}

bool extract_depth(const char* const calibrationFiles[3], const char* const inputFiles[3])
{
    bool ok = false;
    FILE *outTxt = NULL;
    int w, h;
    PlainImage inputImages[3] = {0}, calibrationImages[3] = {0};
    PlainImage inputPhase = NULL, calibPhase = NULL, preview = NULL;
    const int confidenceThreshold = 35;

    CHK( loadImages(inputImages, inputFiles, 3) );
    CHK( loadImages(calibrationImages, calibrationFiles, 3) );

    w = inputImages[0]->width;
    h = inputImages[0]->height;

    calibPhase = PlainImage_Create(w, h, sizeof(PhasePixel), 2);
    inputPhase = PlainImage_Create(w, h, sizeof(PhasePixel), 2);

    // Calculate and unwrap input phase
    SynthesizeWrappedPhaseImage(inputPhase, inputImages);
    UnwrapPhaseImage(inputPhase, confidenceThreshold);

    // Calculate and unwrap calibration phase
    SynthesizeWrappedPhaseImage(calibPhase, calibrationImages);
    UnwrapPhaseImage(calibPhase, confidenceThreshold);

    PhaseImageSubtractInPlace(inputPhase, calibPhase);

    float minVal, maxVal;
    PhaseImageRange(inputPhase, &minVal, &maxVal);
    preview = PlainImage_Create(w, h, 2, 2);
    PhaseImageToGrayAlpha(preview, inputPhase, minVal, maxVal, confidenceThreshold);
    CHK( PlainImage_SavePng(preview, "preview.png") );

#if 1
    CHK( (outTxt = fopen("outcloud.xyz", "w")) != NULL);

    PhaseImageToAscii(outTxt, inputPhase, confidenceThreshold, true, 30.0);
#endif
    ok = true;
cleanup:
    // RAI with PlainImage? Nope, say the savages.
    releaseImages(inputImages, 3);
    releaseImages(calibrationImages, 3);
    PlainImage_Destroy(calibPhase);
    PlainImage_Destroy(inputPhase);
    PlainImage_Destroy(preview);
    if (outTxt)
        fclose(outTxt);
    return ok;
}

int main(int argc, char *argv[])
{
    bool ok = false;
    if (argc == 5 && strcmp(argv[1], "-generate") == 0)
    {
        int width  = atoi(argv[2]);
        int height = atoi(argv[3]);
        float period = atof(argv[4]);
        ok = generate_patterns(width, height, period);
    }
    else if (argc == 8 && strcmp(argv[1], "-depth") == 0)
    {
        ok = extract_depth(argv + 2, argv + 5);
    }
    else
    {
        fprintf(stderr, "Usage: \n"
                "flare -generate <width> <height> <period>\n"
                "\t Generate pattern for projection \n"
                "flare -depth <calib1.png> <calib2.png> <calib3.png> <scan1.png> <scan2.png> <scan3.png>\n"
                "\t Extract depth image \n");
    }
    return (ok) ? EXIT_SUCCESS : EXIT_FAILURE;
}
