#include "fringeprojection.h"
#include <cstdlib>
#include <cassert>
#include <cmath>
#include <algorithm>
#include <cfloat>

typedef FringeProjection::PhasePixel PhasePixel;

template <typename T>
inline T Round(T val)
{
    return floor(val + 0.5);
}

template <typename T>
inline T Max(T a, T b, T c)
{
    return std::max(std::max(a, b), c);
}

template <typename T>
inline T clamp(T minAllowed, T maxAllowed, T val)
{
    return std::max(std::min(val, maxAllowed), minAllowed);
}

bool FringeProjection::GenerateCosinePattern(const PlainImage img, float period, float phaseShift)
{
    assert(img);
    assert(img->bytes_per_sample >= 1);
    int width    = img->width;
    int height   = img->height;

    // Force 8bit grayscale
    if (img->channels != 1 || img->bytes_per_sample != 1)
    {
        img->channels = img->bytes_per_sample = 1;
        img->continuous = false;
    }

    uint8_t *row = img->pixels[0];
    for (int x = 0; x < width; x++) {
        row[x] = 128 + Round(127 * cos((2*M_PI*x)/period + phaseShift));
    }

    // Repeat that row throughout the file
    for (int r = 1; r < height; r++) {
        memcpy(img->pixels[r], img->pixels[0], width * img->bytes_per_sample);
    }

    return img;
}



bool FringeProjection::PhaseImageRange(const PlainImage img, float *minVal, float *maxVal)
{
    assert(img);
    assert(img->bytes_per_sample == sizeof(PhasePixel));
    assert(minVal);
    assert(maxVal);

    float minPhase = FLT_MAX, maxPhase = -FLT_MAX;

    int width = img->width;
    int height= img->height;

    if (img->continuous)
    {
        width  = width * height;
        height = 1;
    }

    for (int r=0; r < height; r++)
    {
        const PhasePixel *row = reinterpret_cast<const PhasePixel*>(img->pixels[r]);
        for (int c = 0; c < width; c++) {
            minPhase = std::min(minPhase, row[c].phase);
            maxPhase = std::max(maxPhase, row[c].phase);
        }
    }

    *minVal = minPhase;
    *maxVal = maxPhase;
    return minPhase <= maxPhase;
}

void FringeProjection::PhaseImageToGrayAlpha(PlainImage dst, const PlainImage img, float minVal, float maxVal, int transparencyThreshold)
{
    assert(img);
    assert(img->bytes_per_sample == sizeof(PhasePixel));

    int width = img->width;
    int height= img->height;

    assert(dst);
    assert(dst->bytes_per_sample == dst->channels);
    assert(dst->channels == 2);
    assert(dst->width  == width);
    assert(dst->height == height);

    // Optimize for continuous images
    if (img->continuous && dst->continuous)
    {
        width  = width * height;
        height = 1;
    }

    const int bbs = dst->bytes_per_sample;
    const float scaling = 255.0f / (maxVal - minVal);
    for (int r=0; r < height; r++)
    {
        const PhasePixel *phaseRow = reinterpret_cast<const PhasePixel*>(img->pixels[r]);
        uint8_t *dstRow = dst->pixels[r];
        for (int c = 0; c < width; c++) {
            int phi = Round(scaling * (phaseRow[c].phase - minVal));
            dstRow[bbs*c] = clamp(0, 255, phi);
            if (transparencyThreshold < 0) {
                dstRow[bbs*c+1] = phaseRow[c].confidence;
            }
            else {
                dstRow[bbs*c+1] = (phaseRow[c].confidence >= transparencyThreshold) ? 255 : 0;
            }
        }
    }
}

void FringeProjection::SynthesizeWrappedPhaseImage(PlainImage wrapped, const PlainImage img[3])
{
    assert(wrapped);
    assert(wrapped->bytes_per_sample == sizeof(PhasePixel));

    bool continuous = wrapped->continuous;
    // We need 3 grayscale images of equal dimensions
    for (int i = 0; i < 3; i++)
    {
        assert(img[i]);
        assert(img[i]->bytes_per_sample == 1);
        assert(img[i]->channels == 1);
        assert(img[i]->width  == wrapped->width);
        assert(img[i]->height == wrapped->height);
        continuous &= img[i]->continuous;
    }

    int width = wrapped->width;
    int height= wrapped->height;

    // Optimize for continuous image
    if (continuous)
    {
        width *= height;
        height = 1;
    }

    const float sqrt3 = sqrtf(3);

    for (int r=0; r < height; r++)
    {
        const uint8_t *row0 = img[0]->pixels[r];
        const uint8_t *row1 = img[1]->pixels[r];
        const uint8_t *row2 = img[2]->pixels[r];
        PhasePixel *dst = reinterpret_cast<PhasePixel*>(wrapped->pixels[r]);

        for (int c = 0; c < width; c++) {
            const int I1 = row0[c];
            const int I2 = row1[c];
            const int I3 = row2[c];
            const int I1_3 = I1 - I3;
            const int I2_1 = I2 - I1;
            const int I2_3 = I2 - I3;
            const float y = sqrt3*(I1_3);
            const float x = (I2_1) + (I2_3);
            dst[c].phase = atan2f(y, x);
            dst[c].confidence = Max( abs(I1_3), abs(I2_1), abs(I2_3) );
            dst[c].rgb[0] = I1;
            dst[c].rgb[1] = I2;
            dst[c].rgb[2] = I3;
        }
    }
}

void FringeProjection::FloydSteinberg(PlainImage destImg, const PlainImage sourceImg)
{
    int width  = sourceImg->width;
    int height = sourceImg->height;

    PlainImage tmp = PlainImage_Create(width + 1, 2, sizeof(short), 1);
    short* px[2] = {(short*) tmp->pixels[0], (short*) tmp->pixels[1]};
    memset(px[0], 0, 2 * tmp->width * tmp->bytes_per_sample);

    for (int r = 0; r < height; r++) {
        // Swap buffers
        short *swp = px[0];
        px[0] = px[1];
        px[1] = swp;

        // Add the source values to the residual error values
        const uint8_t * src = sourceImg->pixels[r];
        for (int c = 0; c < width; c++) {
            px[0][c] += src[c];
        }

        for (int c = 0; c < width; c++) {
            short oldpixel = px[0][c];
            short newpixel  = (oldpixel > 128) ? 255 : 0;
            px[0][c] = newpixel;
            const short error = oldpixel - newpixel;

            px[0][c+1] += (7 * error) / 16;
            px[1][c+1] += error / 16;
            px[1][c-1] += (3 * error) / 16; // If c==0, this safely ..underflows in px[0][width]
            px[1][c]   += (5 * error) / 16;
        }

        // Copy dithered row to output buffer and clear px buffer
        uint8_t *dst = destImg->pixels[r];
        for (int c = 0; c < width; c++) {
            dst[c] = (uint8_t) px[0][c];
            px[0][c] = 0;
        }
    }

    PlainImage_Destroy(tmp);
}

void unwrap2D(PlainImage phase_image, uint8_t min_confidence);

void FringeProjection::UnwrapPhaseImage(PlainImage img, int minConfidence)
{
    unwrap2D(img, minConfidence);
}

void FringeProjection::PhaseImageSubtractInPlace(PlainImage a, const PlainImage b)
{
    assert(a && b);
    assert(a->width  == b->width);
    assert(a->height == b->height);
    assert(a->bytes_per_sample == sizeof(PhasePixel));
    assert(b->bytes_per_sample == sizeof(PhasePixel));

    int width = a->width;
    int height= a->height;

    // Optimize for continuous images
    if (a->continuous && b->continuous)
    {
        width  = width * height;
        height = 1;
    }

    for (int r=0; r < height; r++)
    {
        PhasePixel *row_a = reinterpret_cast<PhasePixel*>(a->pixels[r]);
        const PhasePixel *row_b = reinterpret_cast<const PhasePixel*>(b->pixels[r]);

        for (int c = 0; c < width; c++) {
            row_a[c].phase -= row_b[c].phase;
            row_a[c].confidence = std::min(row_a[c].confidence, row_b[c].confidence);
        }
    }
}


void FringeProjection::PhaseImageToAscii(FILE *out, const PlainImage img, int minConfidence, bool mixdown, float zScale)
{
    assert(img);
    assert(out);
    assert(img->bytes_per_sample == sizeof(PhasePixel));

    int width = img->width;
    int height= img->height;

    for (int iy=0; iy < height; iy++)
    {
        const PhasePixel *row = reinterpret_cast<const PhasePixel*>(img->pixels[iy]);
        for (int ix = 0; ix < width; ix++) {
            if (row[ix].confidence < minConfidence)
                continue;

            int r = row[ix].rgb[0];
            int g = row[ix].rgb[1];
            int b = row[ix].rgb[2];

            if (mixdown) {
                int gray = (r+g+b) / 3;
                r = g = b = gray;
            }

            int y = height/2 - iy;
            int x = ix - width/2;
            float z = -zScale*row[ix].phase;
            fprintf(out, "%d %d %f %d %d %d\n", x, y, z, r, g ,b);
        }
    }
}
