#include "plain_image.h"
#include <png.h>
#include <stdlib.h>
#include <assert.h>

bool PlainImage_SavePng(const PlainImage img, const char *filename)
{
    if( img->is_420v )
    {
        PlainImage rgb = PlainImage_420vToRGB( img );
        bool result = PlainImage_SavePng( rgb, filename );
        PlainImage_Destroy( rgb );
        return result;
    }

    assert( img->channels <= 4 && img->bytes_per_sample==img->channels );

    bool wrote = false;

    if( FILE *file = fopen( filename, "w" ) )
    {
        png_structp png = png_create_write_struct( PNG_LIBPNG_VER_STRING, 0, 0, 0 );
        png_infop info = png ? png_create_info_struct( png ) : 0;

        if( png && info )
        {
            int coltype[5] = { -1, PNG_COLOR_TYPE_GRAY, PNG_COLOR_TYPE_GRAY_ALPHA, PNG_COLOR_TYPE_RGB, PNG_COLOR_TYPE_RGB_ALPHA };

            png_set_IHDR(
                png, info,
                img->width, img->height, 8, coltype[img->channels],
                PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT
            );

            png_init_io( png, file );
            png_set_rows( png, info, img->pixels );
            png_write_png( png, info, PNG_TRANSFORM_IDENTITY, 0 );

            wrote = true;

            png_destroy_write_struct( &png, &info );
        }
        fclose( file );
    }
    return wrote;
}

PlainImage PlainImage_LoadPng(const char *file_name)
{
    if (!file_name) return NULL;

    FILE *fp = fopen(file_name, "rb");
    png_byte header[8];
    if (!fp)
    {
       return NULL;
    }

    size_t bytes_read = fread(header, 1, sizeof(header), fp);
    if (png_sig_cmp(header, 0, bytes_read))
    {
       return NULL;
    }

    png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

    if (!png_ptr)
       return NULL;

    png_infop info_ptr = png_create_info_struct(png_ptr);

    if (!info_ptr)
    {
       png_destroy_read_struct(&png_ptr,
           (png_infopp)NULL, (png_infopp)NULL);
       return NULL;
    }

    png_init_io(png_ptr, fp);
    png_set_sig_bytes(png_ptr, bytes_read);
    png_read_info(png_ptr, info_ptr);

    int width = png_get_image_width(png_ptr, info_ptr);
    int height = png_get_image_height(png_ptr, info_ptr);
    //png_byte color_type = png_get_color_type(png_ptr, info_ptr);
    //png_byte bit_depth = png_get_bit_depth(png_ptr, info_ptr);

    png_set_interlace_handling(png_ptr);
    int channels = png_get_channels(png_ptr, info_ptr);
    png_read_update_info(png_ptr, info_ptr);

    int bytes_per_sample = png_get_rowbytes(png_ptr,info_ptr) / width;
    PlainImage image = PlainImage_Create(width, height, bytes_per_sample, channels);

    png_read_image(png_ptr, image->pixels);

    fclose(fp);
    return image;
}

