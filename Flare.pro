#-------------------------------------------------
#
# Project created by QtCreator 2014-10-25T14:24:31
#
#-------------------------------------------------

QT       -= core gui

TARGET = flare
CONFIG   += console
CONFIG   += warn_on
CONFIG   -= app_bundle

TEMPLATE = app
INCLUDEPATH += /Developer/SDKs/MacOSX10.6.sdk/usr/X11/include
LIBS += -L/Developer/SDKs/MacOSX10.6.sdk/usr/X11/lib -lpng


SOURCES += main.cpp \
    fringeprojection.cpp \
    plain_image.cpp \
    plain_image_pngio.cpp \
    unwrap2D.cpp

HEADERS += \
    fringeprojection.h \
    plain_image.h
